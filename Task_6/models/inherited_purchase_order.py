# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'


    #   TASK 6
    @api.model
    def create(self,vals):
        res = super(PurchaseOrder,self).create(vals)
        if not self.partner_ref:
            res['partner_ref'] = 'No Vendor Reference'
        return res

    @api.multi
    def write(self,vals):
        res = super(PurchaseOrder,self).write(vals)
        if not self.partner_ref:
            self.partner_ref = 'No Vendor Reference'
        return res

    #     @api.model
    # def create(self, vals):
    #     if vals.get('name', 'New') == 'New':
    #         vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order') or '/'
    #     return super(PurchaseOrder, self).create(vals)
  