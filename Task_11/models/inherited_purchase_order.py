# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



    #   TASK 11
class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_cancel(self):
        search_document_obj = self.env['purchase.order']
        search_origin = self.name
        search_document = search_document_obj.search([('origin', '=', search_origin)])
        
        for document in search_document:
            document.write({'state': 'cancel'})
        return super(SaleOrder, self).action_cancel()

    @api.multi
    def action_draft(self):
        search_document_obj = self.env['purchase.order']
        search_origin = self.name
        search_document = search_document_obj.search([('origin', '=', search_origin)])

        orders = self.filtered(lambda s: s.state in ['cancel', 'sent'])
        for document in search_document:
            document.write({'state': 'draft'})
            orders.write({
            'state': 'draft',
            'procurement_group_id': False,})
            orders.mapped('order_line').mapped('procurement_ids').write({'sale_line_id': False})

        return super(SaleOrder, self).action_draft()


