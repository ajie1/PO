# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class StockPicking(models.Model):
    
    _inherit = 'stock.picking'

    # confirm_date =fields.Datetime(string='Confirm Date')
    confirm_date=fields.Datetime(string="Confirmed Date", readonly=True)
    confirm_user=fields.Many2one('res.users',string ='Confirmed By', readonly=True)
    # confirm_user= fields.Many2one("res.users", default = lambda self: self.env.user, string="Confirmed By")

    @api.multi
    def action_confirm(self):
        self.confirm_user= self.env.user
        self.confirm_date= datetime.now()
        return super(StockPicking, self).action_confirm()