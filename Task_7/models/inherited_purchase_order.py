# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class ProductTemplate(models.Model):
    _inherit = 'product.template'


    # TASK 7
    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        if not self.seller_ids:
            supplier_obj = self.env['product.supplierinfo']
            res_partner_obj = self.env['res.partner'].search([('name','=','no supplier')])
            cek = supplier_obj.create({
                'product_tmpl_id' : self.id,
                'name' :res_partner_obj.id,
                'delay':1,
                'min_qty':0,
                'price':0})
                #     print "000000000000000000"
                # print "===================1234"
        return res 
     
    @api.model
    def create(self, vals):
     # vals_supplier =vals['seller_ids']
     # print vals_supplier , "++++++++++++++++="
     # self = self.with_context(dari_create=True) #untuk membuat wadah baru agar tidak mengambil funtion write
        res = super(ProductTemplate, self).create(vals)

        return res