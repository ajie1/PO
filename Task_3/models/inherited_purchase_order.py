# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    #expec_date= fields.string(related='order_line.date_planned')
    duration = fields.Float(string="Duration",compute='_compute_duration')

    @api.one
    @api.depends('date_planned','date_order')
    def _compute_duration(self):
        if not self.date_planned:
            self.date_planned = fields.Date.today()

        date_come = fields.Date.from_string(self.date_planned)
        date_request = fields.Date.from_string(self.date_order)
        calc = date_come - date_request
        self.duration = calc.days