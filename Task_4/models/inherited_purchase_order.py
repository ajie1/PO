# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def action_view_related_products(self):
        list_product = [line.product_id.id for line in self.order_line]

        return{
            'name':'product',
            'view_mode': 'tree',
            'res_model': 'product.product',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain':[('id','in',list_product)]}