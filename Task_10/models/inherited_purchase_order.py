# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # TASK 10

    @api.model
    def create(self, vals):
        self= self.with_context(from_create=True)
        res= super(ProductTemplate, self).create(vals)
        if not vals['seller_ids']:
            raise Warning('You should fill in the supplier details, at least one')
        return res
        
     # vals_supplier =vals['seller_ids']
     # print vals_supplier , "++++++++++++++++="
     # self = self.with_context(dari_create=True) #untuk membuat wadah baru agar tidak mengambil funtion write
        # res = super(ProductTemplate, self).create(vals)

    @api.multi
    def write(self, vals):
        seller_ada= False
        if self.env.context.get('from_create') == True:
            seller_ada = True
        elif seller_ada == False:
            res= super(ProductTemplate, self).write(vals)
            if res:
                for data in self:
                    if not data.seller_ids:
                        raise Warning('You should fill in the supplier details, at least one')
            return res

        # if not self.seller_ids:
        #     # raise osv.except_osv(_("Warning!"), _(" Hello Mehdi Mokni !!."))
        #     raise UserError(_('You Should fill in supplier details, at least one'))
        # return super(ProductTemplate, self).write()