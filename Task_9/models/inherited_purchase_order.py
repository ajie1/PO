# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'


    #   TASK 9
    down_payment= fields.Float(string='Down Payment (DP)')
    amount_total_dp = fields.Float(string='Total', compute='_compute_DP')
    

    @api.depends('amount_total')
    def _compute_DP(self):
        self.amount_total_dp = self.amount_total - self.down_payment
        # l_amount_total = self.amount_total
        # l_down_payment = self.down_payment
        # sums= l_amount_total - l_down_payment
        # self.amount_total_dp = sums