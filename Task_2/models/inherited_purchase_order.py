# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    user= fields.Many2one("res.users", default = lambda self: self.env.user, string="Created")

    